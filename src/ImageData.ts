export class ImageData
{
    id?: string;
    fileName: string;
    buffer?: Buffer;
}

## [3.0.2](https://gitlab.com/mfgames-writing/mfgames-writing-weasyprint-js/compare/v3.0.1...v3.0.2) (2018-08-11)


### Bug Fixes

* adding package management ([3e99a1a](https://gitlab.com/mfgames-writing/mfgames-writing-weasyprint-js/commit/3e99a1a))

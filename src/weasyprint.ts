import { EditionArgs } from "@mfgames-writing/contracts";
import * as child from "child_process";
import * as semver from "semver";
import { WeasyPrintFormatter } from "./WeasyPrintFormatter";

/*
 * This module is working around a number of limitations in WeasyPrint (0.40).
 *
 *   1. We can't inject the page number so all documents start at 1.
 *
 * To get around this, we generate each section with a reset page number as a
 * separate PDF and then combine the results suing pdftk.
 */

export function loadWeasyPrintFormatter(args: EditionArgs)
{
    if (!checkWeasyPrint(args))
    {
        return null;
    }

    if (!checkPdfTk(args))
    {
        return null;
    }

    return new WeasyPrintFormatter();
}

/**
 * Checks the minimum version of WeasyPrint for processing.
 */
function checkPdfTk(args: EditionArgs): boolean
{
    // Get the version from WeasyPrint so we can parse it.
    var cmd = child.spawnSync(
        "pdftk",
        ["--version"],
        { stdio: [0, "pipe", "pipe"] });

    if (!cmd.stdout)
    {
        args.logger.error("Cannot find the `pdftk` executable");

        return false;
    }

    // Old versions of WeasyPrint only used a two-part version which means
    // too old for us.
    var cmdOutput = cmd.stdout.toString().replace(/[\r\n]/g, " ");
    var cmdVersion = cmdOutput.replace(/^.*pdftk (\d+\.\d+).*$/, "$1");

    args.logger.debug("Using PdfTK: " + cmdVersion);

    return !!cmdVersion;
}

/**
 * Checks the minimum version of WeasyPrint for processing.
 */
function checkWeasyPrint(args: EditionArgs): boolean
{
    // Get the version from WeasyPrint so we can parse it.
    var cmd = child.spawnSync(
        "weasyprint",
        ["--version"],
        { stdio: [0, "pipe", "pipe"] });

    if (!cmd.stderr)
    {
        args.logger.error("Cannot find the `weasyprint` executable");

        return false;
    }

    // The most recent version of WeasyPrint (50), writes to stdout (as opposed
    // to stderr) and only has a single number.
    var cmdOutput = cmd.stdout.toString().trim();
    var cmdVersion = cmdOutput.replace(/^.* version (\d+)$/, "$1.0.0");
    var version = semver.valid(cmdVersion);

    args.logger.debug("Using WeasyPrint: " + cmdVersion);

    if (!version)
    {
        args.logger.error(
            "Cannot parse version from `weasyprint` ("
            + cmdVersion
            + "): "
            + cmdOutput);

        return false;
    }

    // Make sure we have our minimum version.
    if (semver.lt(version, "0.42.0"))
    {
        args.logger.error(
            "mfgames-writing-weasyprint requires WeasyPrint 0.42.0 or later"
            + ", found " + cmdVersion);

        return false;
    }

    // We are good.
    return true;
}

export default loadWeasyPrintFormatter;

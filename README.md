# Base PDF Formatter Moonfire Games Writing

This plugin is the base for plugins that create PDF files. This is intended to be extended by other plugins to handle application-specific functionality such as `WeasyPrint` or `wkhtmltopdf`.

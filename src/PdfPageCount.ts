import { EditionArgs } from "@mfgames-writing/contracts";

export interface PdfPageCount
{
    args: EditionArgs;
    count: number;
}
